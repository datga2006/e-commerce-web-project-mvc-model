﻿--------------- HƯỚNG DẪN SỬ DỤNG & QUY TẮC-----------------
_ Mở cmd trong thư mục project, gõ: npm install để cài đặt, sẽ thấy folder node_modules được tải về
_ Khởi động server local trước khi sử dụng: npm start (Thấy thông báo 'Site running on port 3000' là thành công)

+Port server là 3000
+Port mysql là 3306

*Lưu ý: khi push lên bitbucket BẮT BUỘC phải xoá folder "node_modules" rồi mới commit lên server (vì folder rất nặng). Chỉ khi
code mới tải lại như hướng dẫn ở trên.

***** HƯỚNG DẪN CHẠY WEB ******
localhost:3000 hoặc localhost:3000/home : Trang chủ
localhost:3000/products/list : danh sách sản phẩm
localhost:3000/products/detail : chi tiết sản phẩm
localhost:3000/products/pay : thanh toán sản phẩm
localhost:3000/account : account dashboard
localhost:3000/account/order : account order
localhost:3000/account/info : account info
localhost:3000/account/address : account address

*********** QUY TẮC TỔ CHỨC THƯ MỤC *************
_ Folder public: chứa assets và img. Folder trong folder img (ảnh) tên phải rõ ràng như các folder đã có sẵn. Không tạo quá 
nhiều thư mục chứa ảnh (nếu cần thiết mới tạo), ảnh sử dụng riêng cho phần nào thì đặt tên rõ cho phần đó (ví dụ account 
chứa ảnh xài riêng cho account). Folder assets cũng tương tự.
_ Folder views và controller cũng tương tự. 