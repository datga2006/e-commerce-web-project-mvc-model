var express = require('express');
var accountRepo = require('../repos/accountRepo');

var router = express.Router();


router.get('/', (req,res) =>{
	res.locals.user = req.session.user;
	var p1 = accountRepo.loadAccount(res.locals.user.STTTaiKhoan);

	Promise.all([p1]).then(([accountTest]) => {
		res.render('account/user_dashboard', {
			page_title: 'Dashboard',
			account: accountTest[0],
		});
	});
});

router.get('/info', (req,res) =>{
	res.locals.user = req.session.user;
	var p1 = accountRepo.loadAccount(res.locals.user.STTTaiKhoan);

	Promise.all([p1]).then(([myAccount]) => {
		res.render('account/user_info', {
			page_title: 'Login & Security',
			account: myAccount[0]
		});
	});
});

router.get('/address', (req,res) =>{
    res.render('account/user_address', {page_title: 'Your Address'});
});

router.get('/order', (req,res) =>{
	res.locals.user = req.session.user;
	var p1 = accountRepo.loadOrder(res.locals.user.STTTaiKhoan);

	Promise.all([p1]).then(([Order]) => {
		res.render('account/user_order', {
			page_title: 'Your Order',
			order: Order,
			noOrder: Order.length === 0
		});
	});
});

router.get('/order/:sttDonHang', (req,res) =>{
	var sttDonHang = req.params.sttDonHang;
	var p1 = accountRepo.loadOrderDetail(sttDonHang);

	Promise.all([p1]).then(([OrderDetail]) => {
		res.render('account/user_order_detail', {
			page_title: 'Your Order Detail',
			orderdetail: OrderDetail
		});
	});
});

router.get('/changename', (req,res) =>{
	res.render('account/user_change_name', {
			page_title: 'Changing Name',});
});

router.post('/changename', (req, res) => {
	res.locals.user = req.session.user;
	var username = req.body.username;

    accountRepo.updateName(username, res.locals.user.STTTaiKhoan).then(values => {
    	var vm = {
	                showError: true,
	                errorMsg: 'Changing Name Successed!'
            	};
        res.render('account/user_change_name',vm);
    });
});

router.get('/changepassword', (req,res) =>{
	res.render('account/user_change_password', {
			page_title: 'Changing Password',});
});

router.post('/changepassword', (req,res) =>{
	res.locals.user = req.session.user;
	var newpassword = req.body.newpassword;
	var oldpassword = req.body.oldpassword;

	accountRepo.checkpassword(res.locals.user.STTTaiKhoan, oldpassword).then(rows => {
    	if(rows.length > 0)
    	{
    		accountRepo.updatePassword(newpassword, res.locals.user.STTTaiKhoan).then(values => {
    			var vm = {
	                showError: true,
	                errorMsg: 'Changing Password Successed!'
            	};
        		res.render('account/user_change_password',vm);
    		});
    	}else{
    	    var vm = {
               	showError: true,
               	errorMsg: 'Wrong password!'
            };
            res.render('account/user_change_password',vm);
    	}
    });
});

router.get('/changephone', (req,res) =>{
	res.render('account/user_change_phonenumber', {
			page_title: 'Changing Phone Number',});
});

router.post('/changephone', (req, res) => {
	res.locals.user = req.session.user;
	var phonenumber = req.body.phonenumber;
	var password = req.body.password;

    accountRepo.checkpassword(res.locals.user.STTTaiKhoan, password).then(rows => {
    	if(rows.length > 0)
    	{
    		accountRepo.updatePhone(phonenumber, res.locals.user.STTTaiKhoan).then(values => {
    			var vm = {
	                showError: true,
	                errorMsg: 'Changing Phone Number Successed!'
            	};
        		res.render('account/user_change_phonenumber',vm);
    		});
    	}else{
    	    var vm = {
               	showError: true,
               	errorMsg: 'Wrong password!'
            };
            res.render('account/user_change_phonenumber',vm);
    	}
    });
});

router.get('/changeemail', (req,res) =>{
	res.render('account/user_change_email', {
			page_title: 'Changing Email',});
});

router.post('/changeemail', (req, res) => {
	res.locals.user = req.session.user;
	var email = req.body.email;
	var password = req.body.password;

    accountRepo.checkpassword(res.locals.user.STTTaiKhoan, password).then(rows => {
    	if(rows.length > 0)
    	{
    		accountRepo.updateEmail(email, res.locals.user.STTTaiKhoan).then(values => {
    			var vm = {
	                showError: true,
	                errorMsg: 'Changing Email Successed!'
            	};
        		res.render('account/user_change_email',vm);
    		});
    	}else{
    	    var vm = {
               	showError: true,
               	errorMsg: 'Wrong password!'
            };
            res.render('account/user_change_email',vm);
    	}
    });
});

module.exports = router;