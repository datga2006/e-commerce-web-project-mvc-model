var express = require('express');
var adminRepo = require('../repos/adminRepo');

var router = express.Router();

router.get('/admin', (req,res) =>{
    var p1 = adminRepo.loadProductMobile();
    var p2 = adminRepo.loadProductTablet();
    var p3 = adminRepo.loadProductAll();
    var p4 = adminRepo.loadListType();
    var p5 = adminRepo.loadListBrand();
    var p6 = adminRepo.loadListType2();
    var p7 = adminRepo.loadListBrand2();
    var p8 = adminRepo.loadListOrder();
    var p9 = adminRepo.loadStatusOrder();
    Promise.all([p1,p2,p3,p4,p5,p6,p7,p8,p9]).then(([MobileProduct,TabletProduct,ProductAll,Type,Brand,Type2,Brand2,Order,Status]) => {
        res.render('admin/dashboard', {
            page_title: 'Admin Dashboard',
            layout: 'admin.handlebars',
            productsmobile: MobileProduct,
            productstablet: TabletProduct,
            productall: ProductAll,
            type: Type,
            brand: Brand,
            type2: Type2,
            brand2: Brand2,
            listorder: Order,
            statusorder: Status
        });
        console.log(p8);
    });
});

/*router.post('/admin', (req,res) =>{
    var sttSanPham= req.body.sttproduct;
    var tenSP = req.body.nameproduct;
    var sttLoai = req.body.typeedit2;
    var sttHang = req.body.produceredit2;
    var thongTinSP = req.body.infoproduct;
    var giaBan = req.body.priceproduct;
    var soLuong = req.body.amoutproduct;
    console.log(sttSanPham);
    console.log(tenSP);
    console.log(sttLoai);
    console.log(sttHang);
    console.log(thongTinSP);
    console.log(giaBan);
    console.log(soLuong);
    
    adminRepo.updateProduct([sttSanPham, tenSP, sttLoai, sttHang, thongTinSP, soLuong, giaBan]).then(() => {
        res.redirect('/admin');
    });
});
*/
router.post('/admin', (req,res) =>{
    console.log(req.body.addone);
    console.log(req.body.editone);
    if(req.body.addone == "addone")
        {
            var addname = req.body.addnameproduct;
            var addtype = req.body.addtypeproduct;
            var addproducer = req.body.addproducerproduct;
            var addinfo = req.body.addinfoproduct;
            var addprice = req.body.addpriceproduct;
            var addamout = req.body.addamoutproduct;
            var addday = req.body.adddayimportproduct;
            
            adminRepo.addOneProduct(addname, addtype, addproducer, addinfo, addprice, addamout, addday).then(() => {
                res.redirect('admin');
            });

        }
    if(req.body.editone == "editone")
        {
            var sttSanPham= req.body.sttproduct;
            var tenSP = req.body.nameproduct;
            var thongTinSP = req.body.infoproduct;
            var giaBan = req.body.priceproduct;
            var soLuong = req.body.amoutproduct;
           
            adminRepo.updateProduct(sttSanPham, tenSP, thongTinSP, soLuong, giaBan).then(() => {
                res.redirect('admin');
            });
        }
    if(req.body.deleteone == "deleteone")
        {
            var sttSP = req.body.sttproduct;
            console.log(sttSP);
            adminRepo.deleteOneProduct(sttSP).then(() => {
                res.redirect('admin');
            });
        }
    
    if(req.body.okaddproducer == "okaddproducer")
        {
            var thongTin = req.body.addproducerinfo;
            var tenLoai = req.body.addproducername;
            console.log(tenLoai);
            adminRepo.addOneProducer(tenLoai, thongTin).then(() => {
                res.redirect('admin');
            });
        }
    
     if(req.body.okaddtype == "okaddtype")
        {
            var thongTin = req.body.addtypeinfo;
            var tenLoai = req.body.addtypename;
            console.log(tenLoai);
            adminRepo.addOneType(tenLoai, thongTin).then(() => {
                res.redirect('admin');
            });
        }
    if(req.body.deletetype == "deletetype")
        {
            var stttype = req.body.stttype;
            console.log(stttype);
            adminRepo.deleteOneType(stttype).then(() => {
                res.redirect('admin');
            });
        }
    
    if(req.body.okdeleteproducer == "okdeleteproducer")
        {
            var sttPro = req.body.sttproducer;
            console.log(sttPro);
            adminRepo.deleteOneProducer(sttPro).then(() => {
                res.redirect('admin');
            });
        }
    
    if(req.body.okeditproducer == "okeditproducer")
        {
            var sttPro = req.body.sttproducer;
            var tenHang = req.body.editproducername;
            var thongTin = req.body.editproducerinfo;
            console.log(sttPro);
            adminRepo.updateProducer(sttPro,tenHang,thongTin).then(() => {
                res.redirect('admin');
            });
        }
    
    if(req.body.okedittype == "okedittype")
        {
            var sttLoai = req.body.stttype2;
            var tenLoai = req.body.edittypename;
            var thongTin = req.body.edittypeinfo;
            console.log(sttLoai);
            adminRepo.updateType(sttLoai,tenLoai,thongTin).then(() => {
                res.redirect('admin');
            });
        }
    if(req.body.updateorder == "updateorder")
        {
            var order = req.body.orderedit;
            var sttorder = req.body.sttOr;
            console.log(sttorder);
            adminRepo.updateStatus(sttorder, order).then(() => {
                res.redirect('admin');
            });
        }
    
});
module.exports = router;