var express = require('express');
var cartRepo = require('../repos/cartRepo');
var productRepo = require('../repos/productRepo');

var router = express.Router();


router.get('/', (req, res) => {
	console.log('Quantity of products in cart: ' + req.session.cart.length);

	var total = 0;
    var arr_p = [];
    for (var i = 0; i < req.session.cart.length; i++) {
        var cartItem = req.session.cart[i];
        var p = productRepo.single(cartItem.ProId);
        arr_p.push(p);
    }

    var items = [];
    Promise.all(arr_p).then(result => {
        for (var i = result.length - 1; i >= 0; i--) {
            var pro = result[i][0];
            res.locals.item = pro;
            res.locals.item2 = req.session.cart[i];
            total = total + (res.locals.item.GiaBan * res.locals.item2.Quantity);
            var item = {
                Product: pro,
                Quantity: req.session.cart[i].Quantity,
                Amount: pro.GiaBan * req.session.cart[i].Quantity
            };
            items.push(item);
        }

        var vm = {
        	total: total,
            items: items,
            noInfo: req.session.pay === false
        };
        res.render('products/pay', vm);
    });
    console.log(total);
    console.log('Show list products in cart success!');
});



router.post('/', (req, res) => {
	console.log(req.body.deletebutton);
	if(req.body.receiverinfobutton == "receiverinfobutton")
	{
		req.session.pay = true;
		var receiverinfo = {
				ReceiverName: req.body.receivername,
				PhoneNumber: req.body.phonenumber,
				Email: req.body.email,
				Address: req.body.address
			};


		var arr_p = [];
	    for (var i = 0; i < req.session.cart.length; i++) {
	        var cartItem = req.session.cart[i];
	        var p = productRepo.single(cartItem.ProId);
	        arr_p.push(p);
	    }

	    var items = [];
	    Promise.all(arr_p).then(result => {
	        for (var i = result.length - 1; i >= 0; i--) {
	            var pro = result[i][0];
	            var item = {
	                Product: pro,
	                Quantity: req.session.cart[i].Quantity,
	                Amount: pro.GiaBan * req.session.cart[i].Quantity
	            };
	            items.push(item);
	        }

	        var vm = {
	            items: items,
	            receiverinfo: receiverinfo,
	            noInfo: req.session.pay === false
	        };
	        res.render('products/pay', vm);
	    });

		console.log('Change receiver info success!');
	}
	if(req.body.deletebutton == "deletebutton")
	{
		cartRepo.remove(req.session.cart, req.body.productid);
		res.redirect('/cart');
		console.log('Remove product in cart success, product id: ' + req.body.productid);
	}   
	if(req.body.confirmorder == "confirmorder")
	{
		res.locals.user = req.session.user;
		var id = cartRepo.getAutoInc();
		console.log(req.session.userID);
		console.log(req.body.receivername2);
		console.log(req.body.phonenumber2);
		console.log(req.body.address2);

		cartRepo.addOrder(res.locals.user.STTTaiKhoan, req.body.receivername2, req.body.address2).then(values => {
			res.render('products/successorder');
			req.session.pay = false;
			for (var i = 0; i < req.session.cart.length; i++) {
		        var cartItem = req.session.cart[i];
		        productRepo.updateQuantity(cartItem.ProId);
    		}
			req.session.cart = [];
		});
	}
});

module.exports = router;