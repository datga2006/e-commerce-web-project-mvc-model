var express = require('express');
    SHA256 = require('crypto-js/sha256'),
    request = require('request'),
    productRepo = require('../repos/productRepo'),
    accountRepo = require('../repos/accountRepo'),
    restrict = require('../middle-wares/restrict');

var router = express.Router();

router.get('/', (req, res) => {
    req.session.pay = false;
    var p1 = productRepo.latest();
    var p2 = productRepo.best_seller();
    var p3 = productRepo.view();

    Promise.all([p1, p2, p3]).then(([Latest, bestSell, Viewed]) => {
        res.render('home/index', {
            page_title: 'Online Shopping for Mobile & Tablet',
            latest: Latest,
            best_seller: bestSell,
            view: Viewed
        });
    });
});

function validateCaptcha(req, res, next) {
    if (req.body.captcha === undefined || req.body.captcha === '' ||
        req.body.captcha === null
    ) {
        return res.json({
            "success": false,
            "msg": "Please select captcha"
        });
    }
    //Secret Key
    const secretKey = '6LfpImEUAAAAAIGNM9T2kle00X5huhS5VIqDsQCL';
    //Verify URL
    const verifyUrl =
    `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}
    &remoteip=${req.connection.remoteAddress}`;
    
    //Make Request to VerifyURL
    request(verifyUrl, (err, response, body) => {
        body = JSON.parse(body);
        // If not Successful
        if (body.success !== undefined && !body.success) {
            return res.json({
                "success": false,
                "msg": "Captcha validation failed!"
            });
        }
        //If Successful - move to next step
        next();
    });
}

router.post('/signup', validateCaptcha, (req, res, next) => {
    // var dob = moment(req.body.dob, 'D/M/YYYY')
    //     .format('YYYY-MM-DDTHH:mm');

    var user = {
        username: req.body.username,
        email: req.body.email,
        password: SHA256(req.body.password).toString(),
        phone: req.body.phonenumber,
        // dob: dob,
        permission: 0,
    };

    accountRepo.add(user).then(value => {
        res.redirect('/home');
    });
});

router.post('/signin', (req, res) => {
    
    var user = {
        email: req.body.email,
        password: req.body.password
    };

    accountRepo.login(user).then(rows => {
        if (rows.length > 0) {
            req.session.isLogged = true;
            req.session.user = rows[0];
            req.session.cart = [];
            req.session.pay = false;

            var url = '/';
            if (req.query.retUrl) {
                url = req.query.retUrl;
            }
            res.redirect(url);

        } else {
            var vm = {
                showError: true,
                errorMsg: 'Login failed!'
            };
            res.render('home', vm);
        }
        
    });
});

router.post('/logout', (req, res) => {
    req.session.isLogged = false;
    req.session.user = null;
    // req.session.cart = [];
    res.redirect(req.headers.referer);
});

module.exports = router;