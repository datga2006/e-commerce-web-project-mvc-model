var express = require('express');
var productRepo = require('../repos/productRepo');
var cartRepo = require('../repos/cartRepo');
var config = require('../config/config');

var express = require('express'),
    productRepo = require('../repos/productRepo'),
    config = require('../config/config');

var router = express.Router();

router.get('/list/:tenLoai/:tenHang/:sttHang', (req, res) => {
    var sttHang = req.params.sttHang;
    var tenHang = req.params.tenHang;
    var tenLoai = req.params.tenLoai;

    var page = req.query.page;
    if (!page) {
        page = 1;
    }

    var offset = (page - 1) * config.PRODUCTS_PER_PAGE;
    var p1 = productRepo.loadAllByBrand(sttHang, tenLoai, offset);
    var p2 = productRepo.countByBrand(sttHang, tenLoai);

    Promise.all([p1, p2]).then(([pRows, countRows]) => {
        var total = countRows[0].total;
        var nPages = total / config.PRODUCTS_PER_PAGE;
        if (total % config.PRODUCTS_PER_PAGE > 0) {
            nPages++;
        }

        var numbers = [];
        for (i = 1; i <= nPages; i++) {
            numbers.push({
                value: i,
                isCurPage: i === +page
            });
        }

        res.render('products/list', {
            page_title: 'Online Shopping for Mobile & Tablet',
            products: pRows,
            noProducts: pRows.length === 0,
            page_numbers: numbers
        });
    });
});

router.get('/list/:tenLoai', (req, res) => {
    var tenLoai = req.params.tenLoai;

    var page = req.query.page;
    if (!page) {
        page = 1;
    }

    var offset = (page - 1) * config.PRODUCTS_PER_PAGE;
    var p1 = productRepo.loadAllByType(tenLoai, offset);
    var p2 = productRepo.countByType(tenLoai);

    Promise.all([p1, p2]).then(([pRows, countRows]) => {
        var total = countRows[0].total;
        var nPages = total / config.PRODUCTS_PER_PAGE;
        if (total % config.PRODUCTS_PER_PAGE > 0) {
            nPages++;
        }

        var numbers = [];
        for (i = 1; i <= nPages; i++) {
            numbers.push({
                value: i,
                isCurPage: i === +page
            });
        }

        res.render('products/list', {
            page_title: 'Online Shopping for Mobile & Tablet',
            products: pRows,
            noProducts: pRows.length === 0,
            page_numbers: numbers
        });
    });
    
});

router.get('/detail/:sttLoai/:sttHang/:sttSanPham', (req, res) => {
    var sttLoai = req.params.sttLoai;
    var sttSanPham = req.params.sttSanPham;
    var sttHang = req.params.sttHang;

    var p3 = productRepo.same_type(sttLoai);
    var p4 = productRepo.single(sttSanPham);
    var p5 = productRepo.same_brand(sttHang);
    var p6 = productRepo.updateView(sttSanPham);

    Promise.all([p3, p4, p5]).then(([productSameType, rows, productSameBrand, updateView]) => {
        if (rows.length > 0) {
            res.render('products/detail', {
                page_title: 'Online Shopping for Mobile & Tablet',
                product: rows[0],
                product_same_type: productSameType,
                product_same_brand: productSameBrand
            });
        } else {
            res.redirect('/');
        }
    });
});

router.post('/detail/:sttLoai/:sttHang/:sttSanPham', (req,res) =>{
    var id = req.params.sttSanPham;

    if(req.body.AddToCart == "AddToCart")
    {
        var item = {
            ProId: req.body.productid,
            Quantity: req.body.quantity
        };

        cartRepo.add(req.session.cart, item);
        console.log('Add to cart success, product id: ' + id);
        res.redirect('/cart');
    }
    else
    {
        var item = {
            ProId: req.body.addtocart,
            Quantity: 1
        };

        cartRepo.add(req.session.cart, item);
        console.log('Add to cart success, product id: ' + id);
        res.redirect('/cart');
    }
});

router.post('/list/:TenLoai/:TenHang/:page', (req,res) =>{
    var id = req.params.req.body.addtocart;
    var quantity = req.body.quantity;

    var item = {
        ProId: req.params.sttSanPham,
        Quantity: 1
    };

    cartRepo.add(req.session.cart, item);
    console.log('Add to cart success, product id: ' + id);
    res.redirect('/cart');
});

router.get('/search/:temp', (req, res) => {
    var temp = req.params.temp;
    var page = req.query.page;
    if (!page) {
        page = 1;
    }
    
    var offset = (page - 1) * config.PRODUCTS_PER_PAGE;
    var result = productRepo.loadSearch(temp, offset),
        count = productRepo.count(temp);

    Promise.all([result, count]).then(([pRows, countRows]) => {
        var total = countRows[0].total;
        var nPages = total / config.PRODUCTS_PER_PAGE;
        if (total % config.PRODUCTS_PER_PAGE > 0) {
            nPages++;
        }
        var numbers = [];
        for (i = 1; i <= nPages; i++) {
            numbers.push({
                value: i,
                isCurPage: i === +page
            });
        }
        res.render('products/list', {
            page_title: 'Online Shopping for Mobile & Tablet',
            products: pRows,
            noProducts: pRows.length === 0,
            page_numbers: numbers
        });
    });
});

module.exports = router;