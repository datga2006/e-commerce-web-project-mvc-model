var categoryRepo = require('../repos/categoryRepo');

module.exports = (req, res, next) => {
	if (req.session.isLogged === undefined) {
		req.session.isLogged = false;
	}

    categoryRepo.loadUser().then(rows => {
        res.locals.layoutVM = {
            isLogged: req.session.isLogged,
            curUser: req.session.user
        };
        next();
    });
};