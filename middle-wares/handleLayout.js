var categoryRepo = require('../repos/categoryRepo');

var exp = {};

exp.loadALlSp = (req, res, next) => {
    categoryRepo.loadType().then(types => {
        var type = [];
        categoryRepo.loadBrand().then(brands => {
            types.forEach(e => {
                type.push({ name: e.TenLoai, brands: brands });
            });
            res.locals.type = type;
            next();
        });
    });
}

exp.load = (req, res, next) => {
    var p1 = categoryRepo.loadType(),
        p2 = categoryRepo.loadBrand();
        
    Promise.all([p1,p2]).then(([types,brands]) => {
        res.locals.search = {
            Types: types,
            Brands: brands
        };
        next();
    });
}

module.exports = exp;