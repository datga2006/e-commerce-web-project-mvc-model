var modal = document.getElementById("signinModal"),
    signin_btn = document.getElementById("modal-btn"),
    signup = document.getElementById("signupModal"),
    btn_signup = document.getElementById("btnsignup");

// When clicks on the button, open the sign in modal 
signin_btn.onclick = function() {
    modal.style.display = "block";
}

// When clicks on the button, open the sign up modal 
btn_signup.onclick = function() {
    signup.style.display = "block";
}

// When clicks on the button, close the sign in modal
var close1 = document.getElementById("signinClose");
close1.onclick = function(){
    modal.style.display = "none";
}

// When clicks on the button, close the sign up modal
var closeall = document.querySelectorAll("#signupClose");
for ( i in closeall)
{
    closeall[i].onclick = function(){
        signup.style.display = "none";
    }
}

// When clicks anywhere outside of the modal, close it
// window.onclick = function(event) {
//     if (event.target == modal) {
//         modal.style.display = "none";
//     }
//     else if(event.target == signup) {
//         signup.style.display = "none";
//     }
// }

$('input[type="password"]').on('focus', () => {
    $('*').addClass('password');
  }).on('focusout', () => {
    $('*').removeClass('password');
});;