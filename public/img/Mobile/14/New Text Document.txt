https://www.thegioididong.com/dtdd/iphone-8-256gb

<tr>
                            <td style="background-color: #f2f2f2;">Screen</td>
                            <td>LED-backlit IPS LCD, 4.7", Retina HD</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">OS</td>
                            <td>iOS 11</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Back Camera</td>
                            <td>12 MP</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Front Camera</td>
                            <td>7 MP</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">CPU</td>
                            <td>Apple A11 Bionic 6 core</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">RAM</td>
                            <td>2 GB</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">ROM</td>
                            <td>256 GB</td>
                        </tr>
			<tr>
                            <td style="background-color: #f2f2f2;">SIM Card</td>
                            <td>1 Nano SIM</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Battery Capacity</td>
                            <td>1821 mAh</td>
                        </tr>