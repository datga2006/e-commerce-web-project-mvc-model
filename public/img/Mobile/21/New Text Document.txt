https://www.thegioididong.com/dtdd/oppo-a71-32gb-2018

<tr>
                            <td style="background-color: #f2f2f2;">Screen</td>
                            <td>IPS LCD, 5.2", HD</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">OS</td>
                            <td>Android 7.1 (Nougat)</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Back Camera</td>
                            <td>13 MP</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Front Camera</td>
                            <td>5 MP</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">CPU</td>
                            <td>Qualcomm Snapdragon 450 8 nh�n 64-bit</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">RAM</td>
                            <td>3 GB</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">ROM</td>
                            <td>32 GB</td>
                        </tr>
			<tr>
                            <td style="background-color: #f2f2f2;">Memory Card</td>
                            <td>MicroSD, 256 GB</td>
                        </tr>
			<tr>
                            <td style="background-color: #f2f2f2;">SIM Card</td>
                            <td>2 Nano SIM</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Battery Capacity</td>
                            <td>3000 mAh</td>
                        </tr>