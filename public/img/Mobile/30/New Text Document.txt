https://www.thegioididong.com/dtdd/samsung-galaxy-s9-plus-128gb

<tr>
                            <td style="background-color: #f2f2f2;">Screen</td>
                            <td>Super AMOLED, 6.2", Quad HD+ (2K+)</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">OS</td>
                            <td>Android 8.0 (Oreo)</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Back Camera</td>
                            <td>2 camera 12 MP</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Front Camera</td>
                            <td>8 MP</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">CPU</td>
                            <td>Exynos 9810 8 core 64 bit</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">RAM</td>
                            <td>6 GB</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">ROM</td>
                            <td>128 GB</td>
                        </tr>
			<tr>
                            <td style="background-color: #f2f2f2;">Memory Card</td>
                            <td>MicroSD, 400 GB</td>
                        </tr>
			<tr>
                            <td style="background-color: #f2f2f2;">SIM Card</td>
                            <td>2 Nano SIM</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Battery Capacity</td>
                            <td>3500 mAh</td>
                        </tr>