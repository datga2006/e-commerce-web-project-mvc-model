https://www.thegioididong.com/may-tinh-bang/huawei-mediapad-t3-80

<tr>
                            <td style="background-color: #f2f2f2;">Screen</td>
                            <td>IPS LCD, 8"</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">OS</td>
                            <td>Android 7.0</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Back Camera</td>
                            <td>5 MP</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Front Camera</td>
                            <td>2 MP</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">CPU</td>
                            <td>Qualcomm MSM8917, 1.4 GHz</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">RAM</td>
                            <td>2 GB</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">ROM</td>
                            <td>16 GB</td>
                        </tr>
			<tr>
                            <td style="background-color: #f2f2f2;">Network</td>
                            <td>WiFi, 3G, 4G LTE</td>
                        </tr>
			<tr>
                            <td style="background-color: #f2f2f2;">SIM Card</td>
                            <td>Nano SIM</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Battery Capacity</td>
                            <td>4800 mAh</td>
                        </tr>