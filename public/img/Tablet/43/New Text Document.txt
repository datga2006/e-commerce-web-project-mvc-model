https://www.thegioididong.com/may-tinh-bang/lenovo-phab2

<tr>
                            <td style="background-color: #f2f2f2;">Screen</td>
                            <td>IPS LCD, 6.4"</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">OS</td>
                            <td>Android 6.0 (Marshmallow)</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Back Camera</td>
                            <td>13 MP</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Front Camera</td>
                            <td>5 MP</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">CPU</td>
                            <td>MediaTek MT 8735 4 core, 1.3 GHz</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">RAM</td>
                            <td>3 GB</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">ROM</td>
                            <td>32 GB</td>
                        </tr>
			<tr>
                            <td style="background-color: #f2f2f2;">Network</td>
                            <td>WiFi, 3G, 4G LTE</td>
                        </tr>
			<tr>
                            <td style="background-color: #f2f2f2;">SIM Card</td>
                            <td>Micro SIM</td>
                        </tr>
                        <tr>
                            <td style="background-color: #f2f2f2;">Battery Capacity</td>
                            <td>4050 mAh</td>
                        </tr>