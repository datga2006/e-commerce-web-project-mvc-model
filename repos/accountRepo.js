var db = require('../fn/database');

//Test
exports.account = sttTaiKhoan => {
	var sql = `select * from taikhoan where STTTaiKhoan = ${sttTaiKhoan}`;
    return db.load(sql);
}

//Load account info
exports.loadAccount = sttTaiKhoan => {
	var sql = `select * from taikhoan where STTTaiKhoan = ${sttTaiKhoan}`;
    return db.load(sql);
}

//Update account name
exports.updateName = (hoten, sttTaiKhoan) => {
	var sql = `update taikhoan set HoTen = '${hoten}' where STTTaiKhoan = ${sttTaiKhoan}`;
    return db.load(sql);
}

//Update account email
exports.updateEmail = (email, sttTaiKhoan) => {
    var sql = `update taikhoan set Email = '${email}' where STTTaiKhoan = ${sttTaiKhoan}`;
    return db.load(sql);
}

//Update account phone
exports.updatePhone = (phonenumber, sttTaiKhoan) => {
    var sql = `update taikhoan set SDT = '${phonenumber}' where STTTaiKhoan = ${sttTaiKhoan}`;
    return db.load(sql);
}

//Update account password
exports.updatePassword = (password, sttTaiKhoan) => {
    var sql = `update taikhoan set MatKhau = '${password}' where STTTaiKhoan = ${sttTaiKhoan}`;
    return db.load(sql);
}

// Add user regsister info
exports.add = user => {
    var sql = `insert into taikhoan(Email, MatKhau, HoTen, SDT, LoaiTK) 
    values('${user.email}', '${user.password}', '${user.username}', '${user.phone}', 0)`;
    return db.save(sql);
}

//Load data for login
exports.login = user => {
    var sql = `select * from taikhoan where Email = '${user.email}' and MatKhau = '${user.password}' `;
    return db.load(sql);
}

//Check password
exports.checkpassword = (sttTaiKhoan, password) => {
    var sql = `select * from taikhoan where STTTaiKhoan = ${sttTaiKhoan} and MatKhau = '${password}'`;
    return db.load(sql);
}

//Load order
exports.loadOrder = (sttTaiKhoan) => {
    var sql = `SELECT * from donhang join taikhoan on donhang.STTTaiKhoan = taikhoan.STTTaiKhoan WHERE taikhoan.STTTaiKhoan = 1 ORDER BY donhang.STTDonHang DESC`;
    return db.load(sql);
}

//Load order detail
exports.loadOrderDetail = (sttDonHang) => {
    var sql = `SELECT chitietdonhang.STTDonHang, donhang.TinhTrang, chitietdonhang.STTSanPham, sanpham.TenSP, loaisp.TenLoai, sanpham.GiaBan, chitietdonhang.SoLuong FROM chitietdonhang join donhang on chitietdonhang.STTDonHang = donhang.STTDonHang join sanpham on chitietdonhang.STTSanPham = sanpham.STTSanPham join loaisp on loaisp.STTLoai = sanpham.STTLoai WHERE chitietdonhang.STTDonHang = ${sttDonHang}`;
    return db.load(sql);
}


