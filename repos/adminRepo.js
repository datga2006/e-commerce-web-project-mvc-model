var db = require('../fn/database');

// Load all mobile products
exports.loadProductMobile = () => {
	var sql = `select * from sanpham join loaisp on sanpham.STTLoai = loaisp.STTLoai where sanpham.STTLoai = 1`;
    return db.load(sql);
}

//
exports.loadProductTablet = () => {
	var sql = `select * from sanpham join loaisp on sanpham.STTLoai = loaisp.STTLoai where sanpham.STTLoai = 2`;
    return db.load(sql);
}

//
exports.loadProductAll = () => {
	var sql = `select * from sanpham join loaisp on sanpham.STTLoai = loaisp.STTLoai join hangsp on sanpham.STTHang = hangsp.STTHang`;
    return db.load(sql);
}

//load producer section
exports.loadListBrand = () =>{
    var sql = 'select * from hangsp';
    return db.load(sql);
}

//load producer type
exports.loadListType2 = () =>{
    var sql = 'select * from loaisp';
    return db.load(sql);
}

//load producer section
exports.loadListBrand2 = () =>{
    var sql = 'select * from hangsp';
    return db.load(sql);
}


//load producer type
exports.loadListType = () =>{
    var sql = 'select * from loaisp';
    return db.load(sql);
}

exports.addOneProduct = (addname, addtype, addproducer, addinfo, addamout,addprice, adddayimport) =>{
    var sql = `insert into sanpham (TenSP, STTLoai, STTHang, ThongTinSP, SoLuong, GiaBan, NgayNhapHang)
    values('${addname}','${addtype}','${addproducer}','${addinfo}','${addamout}','${addprice}', '${adddayimport}')`;
    return db.save(sql);
}
//Update Product
exports.updateProduct = (sttSanPham, tenSP, thongTinSP, soLuong, giaBan) => {
	var sql = `update sanpham set TenSP = '${tenSP}', ThongTinSP='${thongTinSP}', SoLuong='${soLuong}', GiaBan= '${giaBan}' where sanpham.STTSanPham = ${sttSanPham}`;
    return db.load(sql);
}

//delete 1 product
exports.deleteOneProduct = (sttSP) => {
    var sql = `delete from sanpham where sanpham.STTSanPham = ${sttSP}`;
    return db.load(sql);
}

//add one type
exports.addOneType = (addname, addinfo) =>{
    var sql = `insert into loaisp (TenLoai, ThongTin) values('${addname}','${addinfo}')`;
    return db.save(sql);
}

//add one producer
exports.addOneProducer = (addname, addinfo) =>{
    var sql = `insert into hangsp (TenHang, ThongTin) values('${addname}','${addinfo}')`;
    return db.save(sql);
}

//delete 1 producer
exports.deleteOneProducer = (sttHang) => {
    var sql = `delete from hangsp where hangsp.STTHang = ${sttHang}`;
    return db.load(sql);
}

//delete one type
exports.deleteOneType = (sttType) =>{
    var sql = `delete from loaisp where loaisp.STTLoai = ${sttType}`;
    return db.save(sql);
}
//update producer
exports.updateProducer = (sttHang, tenHang, thongTinHang) => {
	var sql = `update hangsp set TenHang = '${tenHang}', ThongTin='${thongTinHang}' where hangsp.STTHang = ${sttHang}`;
    return db.load(sql);
}

//update type
exports.updateType = (sttLoai, tenLoai, thongTinLoai) => {
	var sql = `update loaisp set TenLoai = '${tenLoai}', ThongTin='${thongTinLoai}' where loaisp.STTLoai = ${sttLoai}`;
    return db.load(sql);
}
//load product type id
exports.loadListBrandId = (sttHang) =>{
    var sql = `select * from sanpham where sanpham.STTHang =${sttHang}`;
    return db.load(sql);
}

//load order
exports.loadListOrder = () =>{
    var sql = `select * from donhang`;
    return db.load(sql);
}
//load order
exports.loadStatusOrder = () =>{
    var sql = `select * from donhang`;
    return db.load(sql);
}
//Update Status
exports.updateStatus = (sttOrder, tinhTrang) => {
	var sql = `update donhang set TinhTrang = '${tinhTrang}' where donhang.STTDonHang = ${sttOrder}`;
    return db.load(sql);
}


