var db = require('../fn/database');

exports.add = (cart, item) => {
    for (i = cart.length - 1; i >= 0; i--) {
        if (cart[i].ProId === item.ProId) {
            cart[i].Quantity += item.Quantity;
            return;
        }
    }

    cart.push(item);
}

exports.remove = (cart, proId) => {
    for (var i = cart.length - 1; i >= 0; i--) {
        if (proId === cart[i].ProId) {
            cart.splice(i, 1);
            return;
        }
    }
}

//Add order
exports.addOrder = (sttTaiKhoan, nguoiNhan, diaChi) => {
	var sql = `INSERT INTO donhang (STTDonHang, STTTaiKhoan, NguoiNhan, DiaChi, TinhTrang) VALUES (NULL, '${sttTaiKhoan}', '${nguoiNhan}', '${diaChi}', 'Being Delivered')`;
    return db.load(sql);
}

//Add detail order
exports.addDetailOrder = (sttDonHang, sttSanPham, soLuong) => {
	var sql = `INSERT INTO chitietdonhang (STTDonHang, STTSanPham, SoLuong) VALUES ('${sttDonHang}', '${sttSanPham}', '${soLuong}')`;
    return db.load(sql);
}

exports.total = (cart) => {
	var total = 0;
    for (i = cart.length - 1; i >= 0; i--) {
        total += cart[i].Amount;
        console.log(total);
    }
    return total;
}

exports.getAutoInc = (sttDonHang, sttSanPham, soLuong) => {
	var sql = `SELECT 'auto_increment' FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'donhang'`;
    return db.load(sql);
}