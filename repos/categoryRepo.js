var db = require('../fn/database');

exports.loadBrand = () => {
    var sql = `select * from hangsp`;
    return db.load(sql);
}

exports.loadType = () => {
    var sql = `select * from loaisp`;
    return db.load(sql);
}

exports.loadUser = () => {
    var sql = 'select * from taikhoan';
    return db.load(sql);
}

//Load products brand
// exports.single = (id) => {
//     return new Promise((resolve, reject) => {
//         var sql = `select * from hangsp where STTHang = ${id}`;
//         db.load(sql).then(rows => {
//             if (rows.length === 0) {
//                 resolve(null);
//             } else {
//                 resolve(rows[0]);
//             }
//         }).catch(err => {
//             reject(err);
//         });
//     });
// }

//Add data
exports.add = (c) => {
    var sql = `insert into loaisp(TenLoai) values('${c.TenLoai}')`;
    return db.save(sql);
}

//Delete data
exports.delete = (id) => {
    var sql = `delete from loaisp where STTHang = ${id}`;
    return db.save(sql);
}

//Update data
exports.update = (c) => {
    var sql = `update loaisp set TenLoai = '${c.TenLoai}' where STTHang = ${c.STTHang}`;
    return db.save(sql);
}
