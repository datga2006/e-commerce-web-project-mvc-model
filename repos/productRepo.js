var db = require('../fn/database');
var config = require('../config/config');

exports.loadAll = () => {
    var sql = 'select * from sanpham';
    return db.load(sql);
}

//Load data by brand with each type
exports.loadAllByBrand = (sttHang, tenLoai, offset) => {
    var sql = `select * from sanpham as sp join loaisp as l on sp.STTLoai = l.STTLoai 
                where STTHang = ${sttHang} and l.TenLoai = '${tenLoai}' limit ${config.PRODUCTS_PER_PAGE} 
                offset ${offset}`;
    return db.load(sql);
}

//Count brand for place product in per page
exports.countByBrand = (sttHang, tenLoai) => {
    var sql = `select count(*) as total from sanpham as sp join loaisp as l on sp.STTLoai = l.STTLoai 
    where STTHang = ${sttHang} and l.TenLoai = '${tenLoai}' `;
    return db.load(sql);
}

//Load data by type
exports.loadAllByType = (tenLoai, offset) => {
    var sql = `select * from sanpham as sp join loaisp as l on sp.STTLoai = l.STTLoai 
            where l.TenLoai = '${tenLoai}' limit ${config.PRODUCTS_PER_PAGE} offset ${offset}`;
    return db.load(sql);
}

//Count type for place product in per page
exports.countByType = tenLoai => {
    var sql = `select count(*) as total from sanpham as sp join loaisp as l on sp.STTLoai = l.STTLoai 
            where l.TenLoai = '${tenLoai}' `;
    return db.load(sql);
}

//Search bar
exports.loadBrand = tenHang => {
    var sql = `select * from hangsp`;
    return db.load(sql);
}

exports.loadType = tenLoai => {
    var sql = `select * from loaisp`;
    return db.load(sql);
}

exports.loadSearch = (name, offset) => {
    var sql = `select * from sanpham as sp join hangsp as h on sp.STTHang = h.STTHang 
    join loaisp as l on sp.STTLoai = l.STTLoai where h.TenHang = '${name}' or l.TenLoai = '${name}'
    limit ${config.PRODUCTS_PER_PAGE} offset ${offset} `;
    console.log(sql);
    return db.load(sql);
}

exports.count = name => {
    var sql = `select count(*) as total from sanpham as sp join hangsp as h on sp.STTHang = h.STTHang 
    join loaisp as l on sp.STTLoai = l.STTLoai where h.TenHang = '${name}' or l.TenLoai = '${name}' `;
    return db.load(sql);
}
//--------------

//-----------For homepage---------------
//Load product detail
exports.single = sttSanPham => {
    var sql = `select * from sanpham join loaisp on sanpham.STTLoai = loaisp.STTLoai join hangsp on sanpham.STTHang = hangsp.STTHang where STTSanPham = ${sttSanPham}`;
    return db.load(sql);
}

//Load product latest
exports.latest = () => {
    var sql = `select * from sanpham join loaisp on sanpham.STTLoai = loaisp.STTLoai order by NgayNhapHang DESC limit ${config.PRODUCTS_HOME}`;
    return db.load(sql);
}

//Load product best seller
exports.best_seller = () => {
    var sql = `select * from sanpham join loaisp on sanpham.STTLoai = loaisp.STTLoai order by LuotMua DESC limit ${config.PRODUCTS_HOME}`;
    return db.load(sql);
}

//Load product most viewed
exports.view = () => {
    var sql = `select * from sanpham join loaisp on sanpham.STTLoai = loaisp.STTLoai order by LuotXem DESC limit ${config.PRODUCTS_HOME}`;
    return db.load(sql);
}
//-----------------------------------------

//Load product same type
exports.same_type = sttLoai => {
    var sql = `select * from sanpham join loaisp on sanpham.STTLoai = loaisp.STTLoai where sanpham.STTLoai = ${sttLoai} limit ${config.PRODUCTS_DETAIL}`;
    return db.load(sql);
}

//Load product same brand
exports.same_brand = sttHang => {
    var sql = `select * from sanpham join hangsp on sanpham.STTHang = hangsp.STTHang join loaisp on sanpham.STTLoai = loaisp.STTLoai where hangsp.STTHang = ${sttHang} limit ${config.PRODUCTS_DETAIL}`;
    return db.load(sql);
}

//Get type from product
exports.get_type = sttSP => {
    var sql = `select STTLoai from sanpham where sanpham.STTSanPham = ${sttSP}`;
    return db.load(sql);
}

// exports.loadName = tenHang => {
//     var sql = `select TenHang from hangsp where TenHang = ${tenHang}`;
//     return db.load(sql);
// }

//Update quantity and quantity of product sold
exports.updateQuantity = sttSanPham => {
    var sql = `UPDATE sanpham SET sanpham.SoLuong = sanpham.SoLuong - 1, sanpham.LuotMua = sanpham.LuotMua + 1 WHERE sanpham.STTSanPham = ${sttSanPham}`;
    return db.load(sql);
}

//Update views
exports.updateView = sttSanPham => {
    var sql = `UPDATE sanpham SET sanpham.LuotXem = sanpham.LuotXem + 1 WHERE sanpham.STTSanPham = ${sttSanPham}`;
    return db.load(sql);
}
